#!/bin/sh

HOST=$1
PATH=$2

grep -v $HOST ~/.ssh/known_hosts > ~/.ssh/known_hosts
nix run github:nix-community/nixos-anywhere -- --flake "${PATH}#digitalocean" "root@${HOST}"
